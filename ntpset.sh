#!/bin/bash

#set -xv

timesyncd_conf="/etc/systemd/timesyncd.conf"

if [ ! -x "$(command -v ntpdate)" ]; then
  exit 0
fi

ntp_active="$(systemctl show -p ActiveState --value ntp)"

if [ "$ntp_active" = "active" ] ; then
  systemctl stop ntp
fi

if [ "${1:-}" != "" ]; then
  servers=$*
else
  if [ ! -e "$timesyncd_conf" ]; then
    exit 0
  fi
  servers=$(grep 'NTP=' "$timesyncd_conf" | grep -F . | tr ' ' '\n' | sed 's/[^=]\+=//g')
fi

for server in $servers ; do
	ntpdate "$server"
  exit_state=$?
  if [ "$exit_state" -eq 0 ]; then
	  break
	fi
done

if [ "$ntp_active" = "active" ] ; then
  systemctl start ntp
fi

exit "$exit_state"


