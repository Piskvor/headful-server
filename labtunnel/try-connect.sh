#!/bin/bash

set -euo pipefail

HOST=$1
shift

for i in 2 3 4 5 ; do
  # we *do* wish to expand this clientside
  # shellcheck disable=SC2029
  if ssh "${HOST}$i" "$@" ; then
    exit
  fi
done
