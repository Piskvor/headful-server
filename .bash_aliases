#!/bin/bash
# bashsupport disable=BP5001,BP5006

alias pass='keepassxc.cli diceware'
alias testgpg="echo \"test\" | gpg --clearsign"
alias killgpg="gpgconf --kill gpg-agent"

alias cl='echo -e "\033c" ; stty sane; setterm -reset; reset; tput reset; clear'

alias vpn='sudo ~/bin/vpn'

alias sidp=sudo
alias sudp=sudo
alias sido=sudo

alias lo=libreoffice
alias xo=xdg-open

alias vin=vim
alias vi,=vim
alias im=vim

alias c=composer
alias co=composer
alias com=composer
alias comp=composer
alias compo=composer
alias compos=composer
alias compose=composer

alias coin='composer install --no-interaction'

alias dokcer=docker
alias dpcker=docker
alias ocker=docker
alias dcker=docker
alias ddocker=docker
alias d=docker

alias doco='docker compose'
alias dodo='docker compose'
alias dc='docker compose'

alias dropbox='ddropbox'
alias lrs='less -R -S +G'

alias cde='cd'
alias xt='xat --encoding=png/L --speaker=off --microphone=off --webcam=off'

alias did="env LANG=C LC_ALL=C vim +'normal Go' +'r!date' ~/did.txt"
alias slowrsync='rsync -avP --times --no-perms --no-group --no-owner --stats --itemize-changes --inplace --bwlimit=1000'
alias gzip='pigz'

# fat fingers - alles ist git!
alias gi='git'
alias igt='git'
alias it='git'
alias gti='git'
alias ggit='git'
alias gut='git'
alias gitt='git'
alias giut='git'
alias gtit='git'
alias fit='git'
alias got='git'
alias g='git'

alias gd='git diff --color-words'
alias gs='git status'
alias gv='git verify-commit'
alias gb='git branch'
alias gco='git checkout'

alias l='git log'
alias gl='git log'
alias 1='git log -1'

alias gf='git fetch'

alias ga='git add'
alias add='git add'

alias commit='git commit'
alias gc='git commit'

alias pull='git pull'

alias gs='git status'
alias status='git status'
alias s='git status'

alias night='git checkout nightly'
alias master='git checkout master'
alias gp='git pull'

# shellcheck disable=SC2154
alias proxy='export http_proxy=http://localhost:13128/;export https_proxy=$http_proxy'
alias noproxy='export http_proxy= ;export https_proxy=$http_proxy'
alias mcb='mc -bs'
alias tvoc='tail -F /var/log/syslog'

alias ss='screen -xR'

alias l='ls -CF'
alias latr='ls -latr'
alias ll='ls -l'
alias la='ls -A'

alias sl='ls'
alias sxc='screen -xR'
alias scx='screen -xR'
alias vss='ssh vagrant'

alias gw='$(yarn bin)/gulp watch --no-uglify'
alias motd='cat /var/run/motd.dynamic'

# if not found agent, look for it again
# bashsupport disable=BP2001
ssh-reagent () {
  for agent in /tmp/ssh-*/agent.*; do
      export SSH_AUTH_SOCK=$agent
      if ssh-add -l 2>/dev/null > /dev/null; then
         echo Found working SSH Agent:
         ssh-add -l
         return
      fi
  done
  echo Cannot find ssh agent - maybe you should reconnect and forward it?
}

alias publicip='dig +short myip.opendns.com @resolver1.opendns.com -4'


