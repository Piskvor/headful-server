#!/bin/sh

set -eu
set -x

shellcheck --shell=bash .bash* .profile ./**/*.sh ./*.sh
