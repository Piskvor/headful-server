#!/bin/bash

set -uo pipefail
set -x

SSHD_CONFIG_FILE=/etc/ssh/sshd_config
SSH_CONFIG_FILE=/etc/ssh/ssh_config

sed -i 's~#HostKey /etc/ssh/ssh_host_rsa_key~HostKey /etc/ssh/ssh_host_rsa_key~' "${SSHD_CONFIG_FILE}"
sed -i 's~^HostKey /etc/ssh/ssh_host_dsa_key~#HostKey /etc/ssh/ssh_host_dsa_key~' "${SSHD_CONFIG_FILE}"
sed -i 's~^HostKey /etc/ssh/ssh_host_ecdsa_key~#HostKey /etc/ssh/ssh_host_ecdsa_key~' "${SSHD_CONFIG_FILE}"
sed -i 's~#HostKey /etc/ssh/ssh_host_ed25519_key~HostKey /etc/ssh/ssh_host_ed25519_key~' "${SSHD_CONFIG_FILE}"

sed -i 's~#PermitRootLogin prohibit-password~PermitRootLogin prohibit-password~' "${SSHD_CONFIG_FILE}"
sed -i 's~PermitRootLogin yes~PermitRootLogin prohibit-password~' "${SSHD_CONFIG_FILE}"

sed -i 's~#PubkeyAuthentication yes~PubkeyAuthentication yes~' "${SSHD_CONFIG_FILE}"
sed -i 's~#PasswordAuthentication no~PasswordAuthentication no~' "${SSHD_CONFIG_FILE}"
sed -i 's~PasswordAuthentication yes~PasswordAuthentication no~' "${SSHD_CONFIG_FILE}"

sed -i 's~AcceptEnv.*~AcceptEnv X_BOGUS~' "${SSHD_CONFIG_FILE}"

sed -i 's~^KexAlgorithms.*~~' "${SSHD_CONFIG_FILE}"
echo 'KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr' >> "${SSHD_CONFIG_FILE}"

awk '$5 > 2000' /etc/ssh/moduli > "${HOME}/moduli"
if [[ "$(wc -l < "${HOME}/moduli")" -gt 0 ]] ; then
  mv "${HOME}/moduli" /etc/ssh/moduli
else
  ssh-keygen -G /etc/ssh/moduli.all -b 4096
  ssh-keygen -T /etc/ssh/moduli.safe -f /etc/ssh/moduli.all
  mv /etc/ssh/moduli.safe /etc/ssh/moduli
  rm /etc/ssh/moduli.all
fi

echo 'Host *
    PasswordAuthentication no
    ChallengeResponseAuthentication no
    PubkeyAuthentication yes
    HostKeyAlgorithms ssh-ed25519-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ssh-ed25519,ssh-rsa
    Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
    MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com' >> "${SSH_CONFIG_FILE}"
