#!/bin/bash

set -x

# shellcheck disable=SC2046
apt install $(sed 's/install//g' < ./package-lists/base-packages.txt | tr '\n' ' ')
